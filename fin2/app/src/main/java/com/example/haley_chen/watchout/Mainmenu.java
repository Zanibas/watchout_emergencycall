package com.example.haley_chen.watchout;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.DeckOfCardsLauncherIcon;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.util.ParcelableUtil;

import java.io.InputStream;


public class Mainmenu extends ActionBarActivity {
    private String phone_number = "1-310-220-9956";
    private String message = "What BE YONDER?";
    private int interval = 5;

    private DeckOfCardsManager mDeckOfCardsManager;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;

    private DeckOfCardsEventListener DoCEL;
    private int counter = 0;

    private Bitmap twenty;
    private Bitmap fourty;
    private Bitmap sixty;
    private Bitmap eighty;
    private Bitmap start;

    private CardImage[] mCardImages;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainmenu);

        DoCEL = new DeckOfCardsEventListenerImpl();

        getSupportActionBar().setTitle("WatchOut");
        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        init();

        Button make_call_btn = (Button) findViewById(R.id.emergency_call);
        /*make_call_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                makeCall(phone_number);
            }
        });*/
        make_call_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                install();
                makeCall(phone_number);
            }

        });

        Button quick_checks_btn = (Button) findViewById(R.id.quick_checks);
        quick_checks_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendText(phone_number, message, interval);

            }
        });


    }

    private void install() {
        updateDeckOfCardsFromUI();
        try{
            mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
        }
        catch (RemoteDeckOfCardsException e){
            e.printStackTrace();
            Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();
        }
    }


    private void updateDeckOfCardsFromUI() {

        if (mRemoteDeckOfCards == null) {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        ListCard listCard= mRemoteDeckOfCards.getListCard();
        // Card #1
        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(0);
        simpleTextCard.setHeaderText("Emergency Protocol");
        simpleTextCard.setTitleText("Tap to Begin");
        String[] messages = {"   0 \ntaps noted so far"};

        simpleTextCard.setMessageText(messages);
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setShowDivider(true);

/*
        SimpleTextCard simple2= (SimpleTextCard)listCard.childAtIndex(0);
        simple2.setHeaderText("Check-in");
        simple2.setTitleText("I've arrived!");
        String[] messages2 = {"Stop sending safety checks."};

        simple2.setMessageText(messages2);
        simple2.setReceivingEvents(false);
        simple2.setShowDivider(true);
        */
    }
    //
    // Create some cards with example content
    private RemoteDeckOfCards createDeckOfCards(){

        ListCard listCard= new ListCard();
        SimpleTextCard simpleTextCard= new SimpleTextCard("0");
        listCard.add(simpleTextCard);


        SimpleTextCard simple2= new SimpleTextCard("card1");
        simple2.setHeaderText("Check-in");
        simple2.setTitleText("I've arrived!");
        String[] messages2 = {"Stop sending safety checks."};
        simple2.setMessageText(messages2);
        simple2.setReceivingEvents(false);
        simple2.setShowDivider(true);
        listCard.add(simple2);

        return new RemoteDeckOfCards(this, listCard);
    }

    //    Initialise
    private void init(){

        // Create the resourse store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();
        // Try to retrieve a stored deck of cards
        try {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        catch (Throwable th){
            th.printStackTrace();
        }

        mCardImages = new CardImage[5];
        try{
            mCardImages[0]= new CardImage("card.image.1", getBitmap("a.png"));
            mCardImages[1]= new CardImage("card.image.2", getBitmap("b.png"));
            mCardImages[2]= new CardImage("card.image.3", getBitmap("c.png"));
            mCardImages[3]= new CardImage("card.image.4", getBitmap("d.png"));
            mCardImages[4]= new CardImage("card.image.5", getBitmap("e.png"));
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get picture icon");
            return;
        }

/*
        mRemoteResourceStore.addResource(mCardImages[0]);
        mRemoteResourceStore.addResource(mCardImages[1]);
        mRemoteResourceStore.addResource(mCardImages[2]);
        mRemoteResourceStore.addResource(mCardImages[3]);
        mRemoteResourceStore.addResource(mCardImages[4]);*/
    }





    private class DeckOfCardsEventListenerImpl implements DeckOfCardsEventListener {
        public void onCardOpen(final String cardId){

        }

        @Override
        public void onCardVisible(String s) {
            ListCard listCard= mRemoteDeckOfCards.getListCard();
            removeDeckOfCards();
            removeDeckOfCards();

            counter +=1;


            SimpleTextCard simple3= new SimpleTextCard(Integer.toString(counter));

            //simple3.setHeaderText("Emergency Protocol");
            //simple3.setTitleText("Tap to Continue");

            if (counter == 0) {
                simple3.setTitleText("Tap to Begin");
            }

            String[] messages3 = {""};

            if (counter == 1) {
                messages3[0] = "   " + Integer.toString(counter) + "\ntap noted so far";
                //mRemoteResourceStore.addResource(mCardImages[0]);
                simple3.setCardImage(mRemoteResourceStore,mCardImages[0]);
            }
            else {
                messages3[0] = "   " + Integer.toString(counter) + "\ntaps noted so far";
                if (counter == 2){
                    //mRemoteResourceStore.addResource(mCardImages[1]);
                    simple3.setCardImage(mRemoteResourceStore,mCardImages[1]);
                }
                else if (counter == 3){

                    simple3.setCardImage(mRemoteResourceStore,mCardImages[2]);
                }
                else if (counter == 4){
                   // mRemoteResourceStore.addResource(mCardImages[3]);
                    simple3.setCardImage(mRemoteResourceStore,mCardImages[3]);
                }
                else if (counter == 5){
                    // mRemoteResourceStore.addResource(mCardImages[3]);
                    simple3.setCardImage(mRemoteResourceStore,mCardImages[4]);
                }

            }

            if (counter >= 6) {
                //simple3.setHeaderText("Emergency Protocol");
                simple3.setTitleText("Activated");
                messages3[0] = "Tap again to cancel";
                //mRemoteResourceStore.addResource(mCardImages[4]);
                //simple3.setCardImage(mRemoteResourceStore,mCardImages[4]);
                counter = -1;
                makeCall(phone_number);
            }

            simple3.setMessageText(messages3);
            simple3.setReceivingEvents(true);
            simple3.setShowDivider(true);

            listCard.add(simple3);
            //removeDeckOfCards();





            //make check-in always second
            SimpleTextCard simple2= new SimpleTextCard("card1");
            simple2.setHeaderText("Check-in");
            simple2.setTitleText("I've arrived!");
            String[] messages2 = {"Stop sending safety checks."};
            simple2.setMessageText(messages2);
            simple2.setReceivingEvents(false);
            simple2.setShowDivider(true);
            listCard.add(simple2);

            try {
                mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
                //Toast.makeText(this, "Failed to Create SimpleTextCard", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onCardInvisible(String s) {

        }

        @Override
        public void onCardClosed(String s) {

        }

        @Override
        public void onMenuOptionSelected(String s, String s2) {

        }

        @Override
        public void onMenuOptionSelected(String s, String s2, String s3) {

        }
    }




    private void removeDeckOfCards() {
        ListCard listCard = mRemoteDeckOfCards.getListCard();
        if (listCard.size() == 0) {
            return;
        }

        listCard.remove(0);

        try {
            mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to delete Card from ListCard", Toast.LENGTH_SHORT).show();
        }

    }



    /**
     * @see android.app.Activity#onStart()
     */
    protected void onStart(){
        super.onStart();

        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }


        mDeckOfCardsManager.addDeckOfCardsEventListener(DoCEL);
/*
        mDeckOfCardsManager.addDeckOfCardsEventListener(new DeckOfCardsEventListener() {
            @Override
            public void onCardOpen(String s) {
                // Hack: If card that just opened isn't a notification,
                // assume it is a character chard...
                if (!s.equals("notification")) {
                    Intent intent = new Intent(getApplicationContext(), DrawSomething.class);
                    // Has file been uploaded yet.
                    intent.putExtra("uploaded", false);
                    startActivity(intent);
                }
            }

            @Override
            public void onCardVisible(String s) {

            }

            @Override
            public void onCardInvisible(String s) {

            }

            @Override
            public void onCardClosed(String s) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2, String s3) {

            }
        });
*/

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    public void makeCall(String phone_number) {
        Intent phoneIntent = new Intent(Intent.ACTION_CALL);
        phoneIntent.setData(Uri.parse("tel:" + phone_number));

        try {
            startActivity(phoneIntent);
            finish();
        }
        catch (ActivityNotFoundException e) {
            Toast.makeText(Mainmenu.this,
                    "Call failed. Please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * @interval (int) in milliseconds
     */
    public void sendText(final String phone_number, String message, int interval) {
        final String myMessage = message;
        final int myInterval = interval;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    int STUB_END_CONDITION = 3;
                    for (int count = 1; count <= STUB_END_CONDITION; count++) {
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(phone_number, null,
                                myMessage, null, null);
                        Thread.sleep(myInterval);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
